<?php

/**
 *
 * @author Thomas Abbondi
 */

return call_user_func(function() {
  $consts = array(
    'site_path' => str_replace('\\', '/', dirname(__DIR__)),
  );

  $consts['media_path'] = "{$consts['site_path']}/media";
  $consts['songs_path'] = "{$consts['media_path']}/songs";
  $consts['covers_path'] = "{$consts['media_path']}/covers";

  return $consts;
});
