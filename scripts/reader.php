<?php

/**
 *
 * @author Thomas Abbondi
 */

header('Content-Type: application/json');

$constants = require_once("constants.php");

$media = array();

if (file_exists($constants['songs_path'])) {
  $exclude = array(".", "..", ".gitignore");

  $iterator_songs = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($constants['songs_path']),
    RecursiveIteratorIterator::SELF_FIRST
  );

  foreach ($iterator_songs as $song) {
    $match = str_replace("{$constants['songs_path']}\\", '', $song->__toString());
    if (!$song->isDir()) {
      if (!in_array($match, $exclude)) {
        $media[] = $match;
        // $media[] = array('song' => $match,);
      }
    }
  }

  unset($iterator_songs);
}

echo json_encode(array_values($media));
